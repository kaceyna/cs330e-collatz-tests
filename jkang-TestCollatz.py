#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    # adding 3+ more tests for test_read()

    def test_read1(self):
        s = "12 100\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 12)
        self.assertEqual(j, 100)

    def test_read2(self):
        s = "200 300\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 200)
        self.assertEqual(j, 300)

    def test_read3(self):
        s = "12 12\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 12)
        self.assertEqual(j, 12)

    # ----
    # eval
    # ----

    # all of these should currently fail because we dont have a solution for v yet
    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    # adding 3+ more tests for test_eval()

    def test_eval_5(self):
        v = collatz_eval(7, 6)
        self.assertEqual(v, 17)

    def test_eval_6(self):
        v = collatz_eval(420, 666)
        self.assertEqual(v, 145)

    def test_eval_7(self):
        v = collatz_eval(10, 10)
        self.assertEqual(v, 7)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 9, 10, 20)
        self.assertEqual(w.getvalue(), "9 10 20\n")

    # adding 3+ more tests for test_print()
    def test_print1(self):
        w = StringIO()
        collatz_print(w, 999998, 999999, 259)
        self.assertEqual(w.getvalue(), "999998 999999 259\n")

    def test_print2(self):
        w = StringIO()
        collatz_print(w, 10, 10, 7)
        self.assertEqual(w.getvalue(), "10 10 7\n")

    def test_print3(self):
        w = StringIO()
        collatz_print(w, 201, 210, 89)
        self.assertEqual(w.getvalue(), "201 210 89\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    # adding 3+ more tests for test_solve()
    def test_solve1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve2(self):
        r = StringIO("15 150\n600 733\n201 222\n999 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "15 150 122\n600 733 171\n201 222 115\n999 1000 112\n")

    def test_solve3(self):
        r = StringIO("19 35\n5000 20000\n9000 9999\n7200 99999\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "19 35 112\n5000 20000 279\n9000 9999 260\n7200 99999 351\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
